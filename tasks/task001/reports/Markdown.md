{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Знакомство с Markdown\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Markdown - это язык разметки текстов. Благодаря Markdown тексты легко писать и читать. Многие используют Markdown для написания различной документации, написания различных блогов, а в случае программистов, для описания своих проектов."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Синтаксис"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Как такового единого стандарта не существует и различные версии Markdown могут отличаться в некоторых деталях, одкако есть опредленные базовые элементы, которые одинковые во всех версиях, ниже я приведу примеры."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Различные выделения текста"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Текст в `*..*` или в `_.._` будет наклонным \n",
    "* *Hello world!!!* \n",
    "* _Hello world!!!_     \n",
    "Текст в `**..**` или в `__..__ `будет жирным:\n",
    "* **Hello world**\n",
    "* __Hello world__   \n",
    "Также можно комбинировать:\n",
    "* _**Hello** word!!!_\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Заголовки в Markdown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "В Markdown существует иерархия заголовков. Это значит, что количесвто `#` показывает на какой степени иерархии вы находитесь:\n",
    "#заголовок 1\n",
    "##подзаголовок 1 \n",
    "#заголовок 2\n",
    "##подзаголовок 2\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ссылки в Markdown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Любой url автоматически становится ссылкой https://vk.com/evgeniu.botvinnikov   \n",
    "Так же ссылкой можно сделать любой текст, для этого `[сам текст нужно писать в квадратных скобках]`, а ссылку в ()   \n",
    "[Жека Ботвинников](https://vk.com/evgeniu.botvinnikov)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Цитаты в  Markdown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " Текст в Markdown становится цитатой, если перед ним поставить `>`   \n",
    " >Истинный ученый — это мечтатель, а кто им не является, тот называет себя практиком."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Вставки кода в Markdown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Для вставки целого блока кода используют тройные обратные тики:   \n",
    "```\n",
    "def func(a):\n",
    "    return a**a\n",
    "```\n",
    "Для добавления куска кода в обычную строку нужно использовать одинарные тики:    \n",
    "` print('Hello word!!!')`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Списки"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "В Markdown бывают двух видов:   \n",
    "1. Непронумерованные\n",
    "2. Пронумерованные   \n",
    "   \n",
    "Пожалуй, начнем с первых. Для создание таких списков вначале пункта списка необходимо поставить ` * `, а для создания подпункта нужно сместиться на несколько пробелов и уже после них поставить ` * `   \n",
    " * первый пункт\n",
    " * второй пункт\n",
    "    * подпункт   \n",
    "\n",
    "\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "В пронумеровнных списках вмсето ` * ` необходимо поставить любые числа, а Markdown приконвертации в HTML или другой формат посатвит числа в правильном и последовательном порядке.\n",
    "   1. первый пункт в пронумерованном списке\n",
    "   2. второй пункт в пронумерованном списке\n",
    "      1. первый подпункт в пронумерованном списке"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Заключение"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Благодаря Markdown можно быстро оформить отчет, при этом экономя время и получая удовольствие от использования данной разметки."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
